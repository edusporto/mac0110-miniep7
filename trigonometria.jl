# MAC0110 - MiniEP7
# Eduardo Sandalo Porto - 11796510

using Test

function test()
    @test check_sin(0, 0)
    @test check_sin(1/2, π/6)
    @test check_sin(√2/2, π/4)
    @test check_sin(√3/2, π/3)
    @test check_sin(1, π/2)

    @test check_cos(1, 0)
    @test check_cos(√3/2, π/6)
    @test check_cos(√2/2, π/4)
    @test check_cos(1/2, π/3)
    @test check_cos(0, π/2)

    @test check_tan(0, 0)
    @test check_tan(√3/3, π/6)
    @test check_tan(1, π/4)
    @test check_tan(√3, π/3)
    # Because of the taylor series used to implement
    # the tan function, using angles equal to or greater
    # than π/2 will wield incorrect results.

    println("Tests passed!")
end

function check_sin(value, x)
    return isapprox(value, taylor_sin(x), atol=0.001)
end

function check_cos(value, x)
    return isapprox(value, taylor_cos(x), atol=0.001)
end

function check_tan(value, x)
    return isapprox(value, taylor_tan(x), atol=0.001)
end


# Trigonometric functions

function taylor_sin(x)
    sinal = 1
    ret::BigFloat = 0
    n::BigInt = 1

    for i in 1:20
        ret += sinal * ((x^n) / factorial(n))
        sinal *= -1
        n += 2
    end

    return ret
end

function taylor_cos(x)
    sinal = 1
    ret::BigFloat = 0
    n::BigInt = 0

    for i in 1:20
        ret += sinal * ((x^n) / factorial(n))
        sinal *= -1
        n += 2
    end

    return ret 
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)

    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end

    return abs(A[1])
end

function taylor_tan(x)
    ret::BigFloat = 0

    for i::BigInt in 1:20
        num = 2^(2*i) * (2^(2*i) - 1) * bernoulli(i) * x^(2*i - 1)
        den = factorial(2*i)

        ret += num / den
    end

    return ret
end

